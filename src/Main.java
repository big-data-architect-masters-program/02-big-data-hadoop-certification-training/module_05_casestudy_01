import java.util.*;

public class Main {

    public static void main(String[] args) {
        //create hashmap
        HashMap students = new HashMap();
        // add element
        students.put(1, "Tony");
        students.put(2, "Sarah");
        students.put(4, "Jan");
        students.put(3, "Dara");
        students.put(5, "HongBB");
        System.out.println(students);
        //delete 2 students records
        students.remove(1);
        students.remove(2);
        System.out.println(students);
        //modify the name of the student having id number 3 and 5
        students.replace(3, "Sok");
        students.replace(5, "Som");
        System.out.println(students);
        //compare two students records
        System.out.println(students.containsValue("Tony"));

        //sort students by name and id
        sortHashMapByValues(students);
        System.out.println(students);


    }

    public static LinkedHashMap<Integer, String> sortHashMapByValues(
            HashMap<Integer, String> passedMap) {
        List<Integer> mapKeys = new ArrayList<>(passedMap.keySet());
        List<String> mapValues = new ArrayList<>(passedMap.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<Integer, String> sortedMap =
                new LinkedHashMap<>();

        Iterator<String> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            String val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                String comp1 = passedMap.get(key);
                String comp2 = val;

                if (comp1.equals(comp2)) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }
        return sortedMap;
    }
}
